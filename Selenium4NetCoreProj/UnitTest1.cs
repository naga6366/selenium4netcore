using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.DevTools;
using OpenQA.Selenium.Firefox;
using WebDriverManager;
using WebDriverManager.DriverConfigs.Impl;
/// <summary>
/// Author: Karthik KK
/// Owner : ExecuteAutomation
/// </summary>
namespace Selenium4NetCoreProj
{
   [TestFixture]
    public class Tests
    {
        private IWebDriver _webDriver;

        [SetUp]
        public void SetUp()
        {
           
            ChromeOptions op = new ChromeOptions();
			op.LeaveBrowserRunning = true;
			_webDriver = new ChromeDriver(op);
			_webDriver.Manage().Window.Maximize();

        }

        [TearDown]
        public void TearDown()
        {
            _webDriver.Quit();
        }

        [Test]
        public void Test()
        {
            _webDriver.Navigate().GoToUrl("https://www.google.com");
            Assert.True(_webDriver.Title.Contains("Google"));
        }
    }
}